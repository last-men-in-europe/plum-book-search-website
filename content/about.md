---
title: "About Plum Books"
subtitle: "Plum Books & Senior Executive Service"
description: "Details about the link between the Plum Book positions and the Senior Executive Service. The Plum Books are an index for the Deep State."
date: 2018-05-15T13:42:58+01:00
width: "text container"
icon: "info"
menu: "about"
weight: "1"
---

### What are Plum Books?
Plum Books are published every four years and contain the 'plum' jobs within the Federal Government; it ostensibly gets its name from the color of the cover.

### Why Are They Important?

All positions within the Plum Books also confer membership of the Special Executive Service; this is a status independent of the regular civil service. It was started by Christine Marcy on behalf of foreign entities unknown.

It is the deep state.

Detailed information about Plum Book & SES is available elsewhere [on the internet](/links-elsewhere/).