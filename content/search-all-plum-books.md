---
title: "Search All Books"
subtitle: "1980 — 2016"
description: "A tool to search all Plum Books from 1980 to 2016 with either Google or Bing. Use to find high-level members of Government and SES members."
date: 2018-05-15T13:42:58+01:00
width: "text container"
icon: "search"
menu: "main"
weight: "20"
---
<div class="ui error message">
  <i class="close icon"></i>
  <div class="header">
    Severe Limitations
  </div>
  <ul class="list">
    <li>Only one 'hit' per Plum Book is returned, eg searching <em>John</em> will return only one result in a Plum Book; you will still need to find it manually.</li>
    <li>There seems incomplete coverage; eg. searching for <em>Podesta</em> will not find Skippy in the 2000 Plum Book, but searching for <em>John</em> will.</li>
    <li>Character recognition is often poor; 'a' and 'e' is commonly mixed up.</li>
    <li>See <b>help</b> tab for more information &amp; solutions.</li>
  </ul>
</div>



<div class="ui pointing secondary menu">
  <a class="item active" data-tab="google"><i class="google red icon"></i> Search with Google</a>
  <a class="item" data-tab="microsoft"><i class="microsoft blue icon"></i> Search with Bing</a>
  <a class="item" data-tab="help">Help</a>

</div>

<div class="ui active tab basic segment" data-tab="google">
    {{< google >}}
</div>

<div class="ui tab basic segment" data-tab="microsoft">
    {{< microsoft >}}
</div>



<div class="ui tab basic segment" data-tab="help">
<p>The Plum Books are indexed by two different search engines: Google &amp; Microsoft. The results will differ depending on which company is used.</p>

<p>If the Bing search does work please try again at the start of the month; only a certain amount of queries can be completed per month.</p>

<p>You may need to turn off your Ad Blocker to see the Google Search.</p>

<h3>Limitations</h3>
<ul>
<li>The Bing Index has not yet added the PDF; therefore this search should not be used</li>
<li>Each search will only return one result per Plum Book; eg searching 'John' will return only one match per Plum Book</li>
<li>Searches are intermittent; for example a search for Podesta will return no results whereas a search for John will find Skippy in the 2000 Plum Book.</li>
</ul>
<h3>Solution</h3>
<p>At the present time downloading and searching all PDFs is the only reliable way of searching Plum Books which are not yet included in the <em>Search Recent Plum Books</em> option.</p>
<p>It is possible the PDFs are indexed better by Microsoft &amp; Google the longer this site stays online </p>


</div>


