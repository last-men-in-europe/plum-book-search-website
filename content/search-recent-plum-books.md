---
title: "Search Recent Books"
subtitle: "1996, 2004, 2008, 2012 & 2016"
description: "Search recent Plum Books with this online tool. The tool will return their name along with title, job, location, and more."
date: 2018-05-15T13:42:58+01:00
width: "container"
icon: "search"
menu: "main"
weight: "10"
---

<section id="home">
    <table id="example" class="ui very compact selectable small table">
        <thead>
            <tr>
                <th class="one wide">Year</th>
                <th>Location</th>
                <th class="two wide">Name</th>
                <th class="three wide">Title</th>
                <th>Type</th>
                <th>Pay Plan</th>
                <th>Level</th>
                <th>Year</th>
                <th>Tenure</th>
                <th class="three wide">Agency</th>
                <th class="three wide">Office</th>
                <th>Sub Office</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</section>