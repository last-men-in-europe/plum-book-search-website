---
title: "Download Books"
subtitle: "Download each Plum Book"
description: "Download Plum Books for all years since 1980 in PDF files for viewing or CSV files to import into Excel or database."
date: 2018-05-15T13:42:58+01:00
width: "text container"
icon: "download"
menu: "main"
weight: "90"
---

Each Plum Book can be downloaded as a `PDF` file and read on your computer. These have been 'transcribed' by Google but much of the character recognition is wrong; this makes searching the Plum Books unreliable.

The `CSV` files have been extracted into fields with a Python script; these can be downloaded and opened in a spreadsheet program, such as Excel. Sadly these are only available for some years due to the format the Plum Book was printed in.

<table class="ui very compact table table">
      <thead>

  <tr>
    <th class="ten wide">President</th>
    <th class="two wide">Year</th>
    <th class="two wide">.PDF</th>
    <th class="two wide">.CSV</th>
    <th class="two wide">Summary</th>
  </tr>
    </thead>
  <tbody>
  <tr>
    <td>Jimmy Carter</td>
    <td>1980</td>
    <td><a href="/files/plum-books/1980-plumbook.pdf">PDF</a></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td rowspan="2">Ronald Regan</td>
    <td>1984</td>
    <td><a href="/files/plum-books/1984-plumbook.pdf">PDF</a></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>1988</td>
    <td><a href="/files/plum-books/1988-plumbook.pdf">PDF</a></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>George Bush Sr</td>
    <td>1992</td>
    <td><a href="/files/plum-books/1992-plumbook.pdf">PDF</a></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td rowspan="2">Bill Clinton</td>
    <td>1996</td>
    <td><a href="/files/plum-books/1996-plumbook.pdf">PDF</a></td>
    <td><a href="https://gitlab.com/last-men-in-europe/plum-book-data/raw/master/1996.csv">CSV</a></td>
    <td><a href="/files/summaries/1996-plum-book.xlsx">XLS</a></td>
  </tr>

  <tr>
    <td>2000</td>
    <td><a href="/files/plum-books/2000-plumbook.pdf">PDF</a></td>
    <td></td>
    <td><a href="/files/summaries/2000-plum-book.xlsx">XLS</a></td>
  </tr>
  <tr>
    <td rowspan="2">George Bush Jr</td>
    <td>2004</td>
    <td><a href="/files/plum-books/2004-plumbook.pdf">PDF</a></td>
    <td><a href="https://gitlab.com/last-men-in-europe/plum-book-data/raw/master/2004.csv">CSV</a></td>
    <td><a href="/files/summaries/2004-plum-book.xlsx">XLS</a></td>
  </tr>
  <tr>
    <td>2008</td>
    <td><a href="/files/plum-books/2008-plumbook.pdf">PDF</a></td>
    <td><a href="https://gitlab.com/last-men-in-europe/plum-book-data/raw/master/2008.csv">CSV</a></td>
    <td><a href="/files/summaries/2008-plum-book.xlsx">XLS</a></td>
  </tr>
  <tr>
    <td rowspan="3">Barak Obama</td>
    <td>2012</td>
    <td><a href="/files/plum-books/2012-plumbook.pdf">PDF</a></td>
    <td><a href="https://gitlab.com/last-men-in-europe/plum-book-data/raw/master/2012.csv">CSV</a></td>
    <td><a href="/files/summaries/2012-plum-book.xlsx">XLS</a></td>
  </tr>
  <tr>
    <td>2016</td>
    <td><a href="/files/plum-books/2016-plumbook.pdf">PDF</a></td>
    <td><a href="https://gitlab.com/last-men-in-europe/plum-book-data/raw/master/2016.csv">CSV</a></td>
    <td><a href="/files/summaries/2016-plum-book.xlsx">XLS</a></td>
  </tr>
  <tr>
    <td>2016<sup>1</sup></td>
    <td><a href="/files/plum-books/2016-performance-review.pdf">PDF</a></td>
    <td></td>
    <td><a href="/files/summaries/2016-performance-review.xlsx">XLS</a></td>
  </tr>
</tbody>
  <tfoot>
    <tr>
        <th colspan="5"><em><sup>1</sup> Performance Review Boards published Federal Register / Vol. 81, No. 191; 500 members, all SES.</em></th>
  </tr></tfoot>
</table>

Download all data as a [`.csv`](https://gitlab.com/last-men-in-europe/plum-book-data/raw/master/all-data.csv) or [`.xslx`](https://gitlab.com/last-men-in-europe/plum-book-data/raw/master/all-data.xlsx) file.

Download [all Plum Books as a zip file](/files/plum-books/all-plum-books.zip).

[See the raw files on Gitlab.](https://gitlab.com/last-men-in-europe/plum-book-data/tree/master)
