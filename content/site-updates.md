---
title: Site Updates
subtitle: Log of site updates
description: "A list of updates to this site."
date: 2018-05-15 13:42:58 +0100
width: text container
icon: refresh
menu: "about"
weight: 100
---
### Version 1 - 17 May 2018

Initial version of the website. The site is online and currently getting indexed. This is required so the PDFs are added to the Google & Bing.