---
title: About this Site
subtitle: Everything you wanted to know, and more.
description: "Information about Plum Books and how they're used in the United States Government. Each member listed in the Plum Book is SES."
date: 2018-05-15 13:42:58 +0100
width: text container
icon: "info"
menu: "about"
weight: "1"
---
#### What?

This is a simple website which allows users to easily search the Plum Book.

#### Why

* Because the Plum Book is the Deep State.
* Because searching the Plum Book is made purposefully difficult; someone has something to hide.
* Because everyone listed are members of a the Senior Executive Service.
* Because we get satisfaction from assisting people find noble truths.

#### Who?

This site was made by the Last Men in Europe. We make things.

## Technical Details

### API Quotas

#### Google

This website uses _Google Fusion Tables_ to search the recent Plum Books. Each key stroke in the search box generates a request to the _Google Fusion Tables_ API. We can send **25,000** requests _per day_ to the API.

A researcher who spends hours on the site can exceed this API limit with ease.

The cost of exceeding this limit is $10,000 / per month! If it stops working please come back; the limit resets at midnight Pacific Time.

The solution to this is relatively easy, and costs far less than $10,000.

In fact, it's free.

#### Bing Custom Search

There are two options for searching the old Plum Books: Google or Bing. **1,000** Bing searches cost [£2.982](https://azure.microsoft.com/en-gb/pricing/details/cognitive-services/bing-custom-search/) so it is limited to _1,000_ searches a month.

At the present time _Bing Search_ works without any payment. There is a work around should this change; no functionality will be lost but it won't look as pretty.

### Website Technology

The website is made from `markdown` files stored in a repository on [Gitlab](https://gitlab.com/last-men-in-europe/plum-book-search-website). They are processed by a program named [Hugo](http://gohugo.io) which in turn generates this website.

The general layout is a framework called [Semantic UI](https://semantic-ui.com) and the icons are from [Font Awesome](https://fontawesome.com). The font is [Lato](http://www.latofonts.com/lato-free-fonts/) and hosted by Google.

Data from recent Plum Books has been extracted with Python and uploaded to [Google Fusion Tables](https://fusiontables.google.com/data?docid=1LRbQQjRDV1uIhUrTvBYH1XzJyWqljKLch_P7P4ua). The raw data is stored at [GitLab](https://gitlab.com/last-men-in-europe/plum-book-search-website). The recent Plum Books are searched with Fusion Tables and the results shown with [Datatables.net](Datatables.net) and [jQuery](https://jquery.com).

The _All Plum Books_ option searches the Plum Books with either [Google Custom Search](https://cse.google.com) or [Bing Custom Search](https://customsearch.ai).

We extend our thanks to the people behind these open source projects; their generosity has allowed us to create a website for $1.89.

## Open Source

### Data

Data from the recent Plum Books has been extracted and formatted as a `.csv` file. These can be imported into a spreadsheet program.

You can download it from this website or our Github repository. Since the Plum Book is a publication of the GPO, the data is in the public domain.

[Data is available on Gitlab.](https://gitlab.com/last-men-in-europe/plum-book-data)

### Website

This website is made by Hugo Static Site Generator and uses Semantic UI, JQuery & Datatables.net.

The website is open source and can be downloaded from Github. You are welcome to copy the website in its entirety. The API keys have been left in; please do not use them.

You can also use the website as a template for other projects.

[The website can be downloaded on Gitlab.](https://gitlab.com/last-men-in-europe/plum-book-search-website)

## Contributing

The sole goal of this website is to liberate the data within the Plum Books to give researchers the ability to mine it. That is more-or-less complete.

Should you wish to add something, or change something, you're welcome to send through a pull request to the repository.

Alternatively, if you can write a section explaining the Plum Book for example, please get in contact; we have made editing pages _very_ easy for those whose talents lie beyond a computer.

## Doing Stuff with the Data

<img src="/images/map.png" class="ui centered image">

The `.csv` files have additional data such as Geolocation. It's possible to play with the data and find lots of interesting things. Above is a heat map showing the location of positions with the Plum Books.

The data is also available on [Google Fusion Tables](https://fusiontables.google.com/data?docid=1LRbQQjRDV1uIhUrTvBYH1XzJyWqljKLch_P7P4ua).

If you make anything awesome please let us know as we'd love to put it here.

## Questions 

### I Want a _Real Party_
We have various scans of the Plum Book in our [files section](/files/).

Enjoy!

### Why Are the Scans so Bad?

Public libraries allowed Google and Microsoft to scan their collections an in exchange those companies would release the scans to the public.

Sadly the public scans they released were terrible quality whilst they kept the high-resolution scans for themselves.

### Indexing PDFs

We are at the mercy of Google and Microsoft as to whether they index the PDF files. Therefore this section simply links to the [Plum Book Files](http://plumbook.party/files/plum-books) that are on the website. 

Sometimes the [2000 Plum Book](http://plumbook.party/files/plum-books/2000-plumbook.pdf) is not added.

Please ignore this question; it's just an excuse to link to files.

### Why `.party`?!

Because it's the cheapest domain; only $1.89 a year.