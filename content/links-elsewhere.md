---
title: "Further Reading"
subtitle: "Information about the Plum Books around the Internet"
description: "Find information about the Plum Books from other sources of information. Links to the original Plum Books are listed."
date: 2018-05-15T13:42:58+01:00
width: "text container"
icon: "link"
menu: "about"
weight: "80"
---

### Original Plum Books on the GPO

The Plum Books below are listed on the GPO's website; they should be considered the canonical source.

* [1996](https://www.gpo.gov/fdsys/pkg/GPO-PLUMBOOK-1996/content-detail.html)
* [2000](https://www.gpo.gov/fdsys/pkg/GPO-PLUMBOOK-2000/content-detail.html)
* [2004](https://www.gpo.gov/fdsys/pkg/GPO-PLUMBOOK-2004/content-detail.html)
* [2008](https://www.gpo.gov/fdsys/pkg/GPO-PLUMBOOK-2008/content-detail.html)
* [2012](https://www.gpo.gov/fdsys/pkg/GPO-PLUMBOOK-2012/content-detail.html)
* [2016](https://www.gpo.gov/fdsys/pkg/GPO-PLUMBOOK-2016/content-detail.html)


All Plum Books since 1996 [cataloged at Gov Info](https://www.govinfo.gov/collection/plum-book?path=/gpo/United%20States%20Government%20Policy%20and%20Supporting%20Positions%20(Plum%20Book)/1996). Plum Books prior to 1992 are not on the GPO website.

### Websites
[Americans for Innovation](https://americans4innovation.blogspot.co.uk/2018/03/obama-hired-them-trump-cannot-fire-them.html) -- Detailed information about SES, Serco & the theft of patents.

[Patriots for Truth](https://patriots4truth.org/category/senior-executive-service/) -- Detailed information about SES, Serco & the theft of patents.

[Abel Danger](https://www.abeldanger.org/tag/senior-executive-service/) -- Original exposers of Serco, SES and the role of the Plum Books.
