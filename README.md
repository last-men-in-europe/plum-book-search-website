# Plum Book Search Website

[![pipeline status](https://gitlab.com/last-men-in-europe/plum-book-search-website/badges/master/pipeline.svg)](https://gitlab.com/last-men-in-europe/plum-book-search-website/commits/master)


A simple website to search Plum Books published by the GPO.

It uses Hugo to build the website; the content is generated from `markdown` files.

#### How to Use

To run this website you will need to install [Hugo](https://gohugo.io/). It is available for all platforms, including Windows and Mac OS.

1. Clone this repository or [extract the zip file](https://gitlab.com/last-men-in-europe/plum-book-search-website/-/archive/master/plum-book-search-website-master.zip) into a directory.
2. Change into the directory you created.
3. Run `hugo serve`.
4. Navigate to http://localhost:1313

#### Contribute

Discuss any ideas with us; they are much welcome.

We are keen to add more details about the Plum Books; do contact if you can do this. Forestry.io is enabled so editing is very easy.